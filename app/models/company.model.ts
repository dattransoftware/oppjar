export interface ICompany {
    id: number;
    name: string;
    address: string;
    mapUrl: string;
}

export interface ITransaction {
    category: 'home', 'transport', 'communication', 'hotel', 'restaurant';
    amount: number;
    companyId: number;
    status: 'completed' | 'pending';
    date: Date;
}

export interface IListTransactionItem extends ITransaction {
    itemType: string;
}

export interface IListTransactionHeader {
    itemType: string;
    date: Date;
}