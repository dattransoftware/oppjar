export const env = {
    PRODUCTION: false,
    NODE_ENV: 'develop',
    LANGUAGE: {
        DEFAULT: 'en',
        DIRECTORY: '/assets/i18n/',
        EXTENSION: '.json'
    }
};