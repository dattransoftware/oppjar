/**
* @author Dat Tran
* @date 09/01/2020
* @license MIT License Copyright (c) 2019
*/

import { Component, OnInit } from "@angular/core";
import { isAndroid } from "tns-core-modules/platform";
import { ModalDialogParams } from 'nativescript-angular/modal-dialog';
import * as SocialShare from "nativescript-social-share";

import { TranslateService } from '../../../plugins/@ngx-translate/core@10.0.2';

import { DataService, DateService } from "../../services/index.service";
import { IListTransactionItem, ICompany } from "../../models/index.model";

@Component({
    selector: "oj-modal",
    moduleId: module.id,
    templateUrl: "./modal.component.html"
})
export class ModalComponent implements OnInit {
    public webViewSrc = '';
    data: DataService;
    transactionData: IListTransactionItem;
    categoryIcon: string;
    companyData: ICompany;
    dateFormat: DateService;

    constructor(
      private params: ModalDialogParams,
      private translate: TranslateService
    ) {
      this.data = new DataService();
      this.dateFormat = new DateService();

      this.transactionData = params.context as IListTransactionItem;
      this.categoryIcon = this.data.getCategoriesIcons()[this.transactionData.category];
      this.companyData = this.data.getCompanies().find(i => i.id === this.transactionData.companyId);

      this.webViewSrc = '<!DOCTYPE html><html><body>';
      this.webViewSrc += `<iframe src="${this.companyData.mapUrl}" width="100%" height="200" frameborder="0" style="border:0" allowfullscreen></iframe>`;
      this.webViewSrc += '</body></html>';
    }

    ngOnInit(): void {
      // Init your component properties here.
    }

    public get amount(): string {
      if (this.transactionData.amount >= 0) {
        return `$${this.transactionData.amount}`;
      } else {
        return `- $${Math.abs(this.transactionData.amount)}`;
      }
    }

    public onClose(): void {
      this.params.closeCallback();
    }

    public onShare(): void {
      SocialShare.shareText("I love OppJar App, www.oppjar.com!", "How would you like to share this text?");
    }

    public onWebViewLoaded(args) {
      const webview = args.object;
      if (isAndroid) {
        webview.android.getSettings().setDisplayZoomControls(false);
      }
    }

    public formatDate(value: Date) {
      return this.dateFormat.longDate(value, this.translate.currentLang);
    }
}
