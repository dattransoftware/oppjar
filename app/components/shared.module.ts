import { NgModule } from '@angular/core';
import { NativeScriptCommonModule } from 'nativescript-angular/common';

import { TranslateModule } from '../../plugins/@ngx-translate/core@10.0.2';

import { TransactionsListComponent } from './transactions-list/transactions-list.component';
import { ModalComponent } from './modal/modal.component';

@NgModule({
  declarations: [
    TransactionsListComponent,
    ModalComponent
  ],
  imports: [
    NativeScriptCommonModule,
    TranslateModule.forChild()
  ],
  exports: [
    TransactionsListComponent,
    ModalComponent
  ],
  entryComponents: [ModalComponent]
})
export class SharedModule { }
