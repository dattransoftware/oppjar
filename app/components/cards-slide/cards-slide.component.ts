import { Component, Input } from '@angular/core';

@Component({
  selector: 'oj-cards-slide',
  templateUrl: './cards-slide.component.html'
})
export class CardsSlideComponent {
  @Input() data = [];
}
