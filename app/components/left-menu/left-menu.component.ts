import { Component, OnInit } from '@angular/core';
import { SideDrawerService } from '../../services/side-drawer.service';

@Component({
  selector: 'oj-left-menu',
  templateUrl: './left-menu.component.html',
  styleUrls: ['./left-menu.component.scss']
})
export class LeftMenuComponent implements OnInit {

  constructor(
    public sideDrawerService: SideDrawerService
  ) { }

  ngOnInit() {
  }

  public closeDrawer() {
    this.sideDrawerService.closeDrawer();
  }

}
