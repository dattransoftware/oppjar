import { Component, OnInit } from '@angular/core';
import { SideDrawerService } from '../../services/side-drawer.service';

export const ICON_OPEN_MENU = String.fromCharCode(0xf141);

@Component({
  selector: 'oj-action-bar',
  templateUrl: './action-bar.component.html',
  styleUrls: ['./action-bar.component.scss']
})
export class ActionBarComponent implements OnInit {

  public drawerTrigger: string = ICON_OPEN_MENU;

  constructor(
    public sideDrawerService: SideDrawerService
  ) { }

  ngOnInit() {
  }

}
