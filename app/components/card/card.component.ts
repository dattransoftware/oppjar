import { Component, Input } from "@angular/core";

@Component({
    selector: "oj-card",
    moduleId: module.id,
    templateUrl: "./card.component.html"
})
export class CardComponent {
    @Input() data = [];
}
