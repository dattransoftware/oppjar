import { Component, OnInit } from '@angular/core';
import * as platform from 'tns-core-modules/platform';

@Component({
  selector: 'oj-new-items',
  templateUrl: './new-items.component.html',
  styleUrls: ['./new-items.component.scss']
})
export class NewItemsComponent implements OnInit {

  public screenHeight: number;
  public screenWidth: number;

  constructor() {
      this.screenWidth = platform.screen.mainScreen.widthDIPs;
      this.screenHeight = platform.screen.mainScreen.heightDIPs;
  }

  ngOnInit() {
  }

}
