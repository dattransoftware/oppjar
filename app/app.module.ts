import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { NativeScriptModule } from 'nativescript-angular/nativescript.module';
import { NativeScriptCommonModule } from "nativescript-angular/common";
import { registerElement } from 'nativescript-angular/element-registry';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { NativeScriptFormsModule } from 'nativescript-angular/forms';
import { NativeScriptHttpClientModule } from 'nativescript-angular/http-client';
import { NativeScriptUISideDrawerModule } from "nativescript-ui-sidedrawer/angular";

import { AppComponent } from './app.component';
import { TranslateModule } from '../plugins/@ngx-translate/core@10.0.2';
import { BottomNavigationComponent } from './components/bottom-navigation/bottom-navigation.component';

registerElement("PullToRefresh", () => require("@nstudio/nativescript-pulltorefresh").PullToRefresh);

@NgModule({
  declarations: [
    AppComponent,
    BottomNavigationComponent,
  ],
  imports: [
    AppRoutingModule,
    NativeScriptModule,
    NativeScriptCommonModule,
    NativeScriptHttpClientModule,
    NativeScriptUISideDrawerModule,
    HttpClientModule,
    NativeScriptFormsModule,
    TranslateModule.forRoot()
  ],
  bootstrap: [
    AppComponent
  ],
  schemas: [NO_ERRORS_SCHEMA]
})
export class AppModule {}

