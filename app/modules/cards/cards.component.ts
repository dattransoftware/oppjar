/**
 * @author Dat Tran
 * @license MIT License Copyright (c) 2020
 */

import { Component, OnInit } from '@angular/core';
import { DataService } from '../../services/index.service';

@Component({
  selector: 'Cards',
  templateUrl: './cards.component.html'
})
export class CardsComponent implements OnInit {

  data: DataService;
  transactions = [];

  constructor() {
    this.data = new DataService();

    this.transactions = this.data.getTransactions();
  }

  ngOnInit(): void {
    // Init your component properties here.
  }

}
