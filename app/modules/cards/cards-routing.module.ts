/**
* @author Dat Tran
* @license MIT License Copyright (c) 2020
*/

import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";

import { CardsComponent } from "./cards.component";

const routes: Routes = [
    { path: "", component: CardsComponent }
];

@NgModule({
    imports: [NativeScriptRouterModule.forChild(routes)],
    exports: [NativeScriptRouterModule]
})
export class CardsRoutingModule { }
