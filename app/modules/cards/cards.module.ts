import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { NativeScriptCommonModule } from 'nativescript-angular/common';
import { CardsComponent } from './cards.component';
import { NativeScriptUISideDrawerModule } from "nativescript-ui-sidedrawer/angular";
import { NativeScriptUIListViewModule } from "nativescript-ui-listview/angular";
import { NativeScriptUICalendarModule } from "nativescript-ui-calendar/angular";
import { NativeScriptUIChartModule } from "nativescript-ui-chart/angular";
import { NativeScriptUIDataFormModule } from "nativescript-ui-dataform/angular";
import { NativeScriptUIAutoCompleteTextViewModule } from "nativescript-ui-autocomplete/angular";
import { NativeScriptUIGaugeModule } from "nativescript-ui-gauge/angular";
import { NativeScriptFormsModule } from "nativescript-angular/forms";

import { TranslateModule } from '../../../plugins/@ngx-translate/core@10.0.2';
import { SharedModule } from '../../components/shared.module';
import { CardsRoutingModule } from "./cards-routing.module";
import { CardComponent } from '../../components/card/card.component';
import { CardsSlideComponent } from '../../components/cards-slide/cards-slide.component';

@NgModule({
  declarations: [
    CardsComponent,
    CardComponent,
    CardsSlideComponent
  ],
  imports: [
    NativeScriptUISideDrawerModule,
    NativeScriptUIListViewModule,
    NativeScriptUICalendarModule,
    NativeScriptUIChartModule,
    NativeScriptUIDataFormModule,
    NativeScriptUIAutoCompleteTextViewModule,
    NativeScriptUIGaugeModule,
    NativeScriptCommonModule,
    CardsRoutingModule,
    NativeScriptFormsModule,
    TranslateModule.forChild(),
    SharedModule
  ],
  schemas: [NO_ERRORS_SCHEMA]
})
export class CardsModule { }
