/**
* @author Dat Tran
* @license MIT License Copyright (c) 2020
*/

import { Component, OnInit } from "@angular/core";

import { DataService } from "../../services/index.service";
import { TranslateService, LangChangeEvent } from '../../../plugins/@ngx-translate/core@10.0.2';

@Component({
    selector: "Home",
    moduleId: module.id,
    templateUrl: "./home.component.html"
})
export class HomeComponent implements OnInit {
  expensesCategories = [
    {
      color: 'red',
      label: 'Home'
    },
    {
      color: 'blue',
      label: 'Auto & Transport'
    },
    {
      color: 'green',
      label: 'Cellular'
    },
    {
      color: 'orange',
      label: 'Hotel & Restaurant'
    }
  ];

  data: DataService;
  transactions = [];
  expensesChartDataEn = [];
  expensesChartDataEs = [];

  public currentLanguage = 'en';

  constructor(private translate: TranslateService) {
    this.data = new DataService();

    this.transactions = this.data.getTransactions();

    this.expensesChartDataEn = [
      { name: "Home", amount: 90 },
      { name: "Auto & Transport", amount: 76 },
      { name: "Communication", amount: 60 },
      { name: "Hotel", amount: 44 }
    ];

    this.expensesChartDataEs = [
      { name: "Hogar", amount: 20 },
      { name: "Transporte", amount: 76 },
      { name: "Comunicación", amount: 60 },
      { name: "Hotel", amount: 44 }
    ];

    translate.onLangChange.subscribe((event: LangChangeEvent) => {
      this.currentLanguage = event.lang;
    });
  }

  ngOnInit(): void {
    // Init your component properties here.
  }
}
