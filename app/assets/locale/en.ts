/**
* @author Dat Tran
* @license MIT License Copyright (c) 2020
*/

export default {
    // General
    TRANSACTIONS: 'Transactions',
    CARDS: 'Cards',
    MENU: 'Menu',

    sidedrawer: {
        balance: 'Balance',
        english: 'English',
        spanish: 'Español',
        madeBy: 'Made with ❤ by ',
    },

    balance: {
        income: 'INCOME',
        expenses: 'EXPENSES',
        balance: 'BALANCE',
    },

    cards: {
        title: 'Cards'
    },

    categories: {
        home: 'Home',
        transport: 'Auto & Transport',
        communication: 'Communication',
        hotel: 'Hotel',
        restaurant: 'Restaurant'
    },

    detail: {
        status: {
            completed: 'Completed',
            pending: 'Pending',
        }
    }
}