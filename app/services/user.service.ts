// The following is a sample implementation of a backend service using Progress Kinvey (https://www.progress.com/kinvey).
// Feel free to swap in your own service / APIs / etc here for your own apps.

import { Injectable } from "@angular/core";
import { User } from "../models/user.model";

@Injectable()
export class UserService {
    constructor() { }

    register(user: User) {
        return true;
    }

    login(user: User) {
        return true;
    }

    logout() {
        return true;
    }

    resetPassword(email) {
        return true;
    }

    handleErrors(error: any) {
        console.error(error.message);
        return Promise.reject(error.message);
    }
}
