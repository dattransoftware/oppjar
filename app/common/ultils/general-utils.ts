import * as connectivity from "tns-core-modules/connectivity";
import * as Toast from "nativescript-toast";
const LoadingIndicator = require("nativescript-loading-indicator").LoadingIndicator;
const loadingIndicator = new LoadingIndicator();
const loadingIndicatorOptions = {
    ios: {
        dimBackground: true
    }
};

export function checkInternetConnection() {
    let connectionType = connectivity.getConnectionType();
    if (connectionType === connectivity.connectionType.none) {
        return false;
    }
    return true;
}

export function showToast(message: string, longTime?: boolean) {
    if (longTime) {
        Toast.makeText(message, Toast.duration.long[0]);
    }
    else {
        Toast.makeText(message).show();
    }
}

export function showLoadingIndicator(message?: string) {
    loadingIndicator.show({
        message: message,
        ios: loadingIndicatorOptions.ios
    });
}

export function hideLoadingIndicator() {
    loadingIndicator.hide();
}

