export const BASE_API_URL = 'http://localhost:3000/';

export const API_URL = {
    LOGIN: 'login'
};

export const REQUEST_TYPE = {
    GET: 'GET',
    POST: 'POST',
    PUT: 'PUT',
    DEL: 'DELETE'
};