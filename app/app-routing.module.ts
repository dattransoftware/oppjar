import { NgModule } from '@angular/core';
import { NativeScriptRouterModule } from 'nativescript-angular/router';
import { Routes } from '@angular/router';

const routes: Routes = [
  { path: "", redirectTo: "/login", pathMatch: "full" },
  { path: "login", loadChildren: "./modules/authentication/login/login.module#LoginModule" },
  { path: "home", loadChildren: "./modules/home/home.module#HomeModule" },
  { path: "cards", loadChildren: "./modules/cards/cards.module#CardsModule" },
];

@NgModule({
  imports: [NativeScriptRouterModule.forRoot(routes)],
  exports: [NativeScriptRouterModule]
})
export class AppRoutingModule { }
